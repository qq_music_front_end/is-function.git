import isFunction from './index';
import JSObject from 'js-object-stringify';

//#region 测试用例
const cases = [
    {
        data: undefined,
        target: false
    },
    {
        data: null,
        target: false
    },
    {
        data: '',
        target: false
    },
    {
        data: /a/,
        target: false
    },
    {
        data: true,
        target: false
    },
    {
        data: NaN,
        target: false
    },
    {
        data: 10086,
        target: false
    },
    {
        data: {},
        target: false
    },
    {
        data: () => { },
        target: true
    },
    {
        data: async () => { },
        target: true
    },
    {
        data: alert,
        target: true
    }
];
//#endregion

describe('isFunction', () => {
    cases.forEach(({ data, target }) => {
        test(JSObject.stringify(data), () => {
            expect(isFunction(data)).toEqual(target);
        });
    });
});
