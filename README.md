# is-function

判断当前值是否为函数

```bash
npm i -S @qmfe/is-function
```

```js
import isFunction from '@qmfe/is-function';

console.assert(isFunction(alert) === true);

console.assert(isFunction(1234) === true);
// Assertion failed: console.assert
```
