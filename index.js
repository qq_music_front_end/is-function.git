/**
 * 判断 `value` 是否是 `Function` 类型
 * 
 * @param {any} value 需要判断的值
 * @returns {boolean} 如果是则返回true，否则返回false
 * @example
 *
 * isFunction(async function() {})
 * // => true
 * 
 * isFunction(function() {})
 * // => true
 *
 * isFunction(/abc/)
 * // => false
 * 
 * isFunction({})
 * // => false
 */

export default function isFunction(value) {
    return typeof value === 'function';
}
